+++
title = "Isabella Wang" 

description = "Data Science Researcher at Duke University"
+++


Self Introduction
-----------

<span style="font-family: Arial;font-size:18px;">

Hi Everyone, Welcome to my Channel! 


I am a Research Assistant at Duke University and the National Institutes of Health (NIH), where I collaborate with the Rare Diseases Informatics group to generate machine learning models for early diagnosis prediction and drug repurposing. I also participate in the AI lab at UCLA, developing an app for generating synthetic datasets while maintaining the privacy of real input data. 

I am pursuing my Master's degree in Computer Engineering in the Machine Learning Track at Duke University, after completing my Bachelor's degree in Statistics with a minor in Mathematics at UCLA. I have extensive experience in applied machine learning, statistical analysis, database management, and hypothesis testing. I am passionate about applying data science to solve real-world problems in the healthcare and finance sectors. 
</span>



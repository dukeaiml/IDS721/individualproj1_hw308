+++
title = "CS671D: Theory of Machine Learning"
description = "Keywords: Regression, Tree, Neural Network, SVM, Transformers, K-means, Kernel, EM "
date = 2024-03-07
template = "non-post-page.html"

[extra]
toc = true
+++
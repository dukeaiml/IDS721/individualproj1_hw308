+++
title = "IDS721: Data Analysis in Cloud Computing"
description = "Keywords: AWS, Rust, Web, S3"
date = 2024-03-08
template = "non-post-page.html"

[extra]
toc = true
+++

#### Mini-Project 1
Create a static site with Zola, a Rust static site generator, that will hold all of the portofolio work in this class.

[Check the Repo for more details](https://gitlab.com/dukeaiml/IDS721/miniproj1_hw308) 

Requirements
- Static site generated with [Zola](https://www.getzola.org)
- Home page and portfolio project page templates
- Styled with CSS
- GitLab repo with source code

#### Mini-Project 2
Create a simple AWS Lambda function that processes data.
It achieved the calculator functionality of subtraction. 

[Check the Repo for more details](https://gitlab.com/dukeaiml/IDS721/miniproj2_hw308) 

Requirements
- Rust Lambda Function using [Cargo Lambda](https://www.cargo-lambda.info)
- Process and transform sample data

#### Mini-Project 3
Create an S3 Bucket using CDK with AWS CodeWhisperer

[Check the Repo for more details](https://gitlab.com/dukeaiml/IDS721/miniproj3_hw308) 

Requirements
- Create S3 bucket using AWS CDK
- Use CodeWhisperer to generate CDK code
- Add bucket properties like versioning and encryption

#### Mini-Project 4
Containerize a Rust Actix Web Service

[Check the Repo for more details](https://gitlab.com/dukeaiml/IDS721/miniproj4_hw308) 

Requirements
- Containerize simple Rust Actix web app
- Build Docker image
- Run container locally

#### Mini-Project 5
Serverless Rust Microservice 

[Check the Repo for more details](https://gitlab.com/dukeaiml/IDS721/miniproj5_hw308) 

Requirements
- Create a Rust AWS Lambda function (or app runner)
- Implement a simple service
- Connect to a database

#### Mini-Project 6
Instrument a Rust Lambda Function with Logging and Tracing

[Check the Repo for more details](https://gitlab.com/dukeaiml/IDS721/miniproj6_hw308) 

Requirements
- Add logging to a Rust Lambda function
- Integrate AWS X-Ray tracing
- Connect logs/traces to CloudWatch


+++
title = "IDS703: Natural Language Processing"
description = "Keywords: Embedding, BERT, Transformer"
date = 2024-03-07
template = "non-post-page.html"

[extra]
toc = true
+++
+++
title = "About Me"
description = "I am a Data Science Researcher and my study focus on the AI application in healthcare sector."
date = 2024-01-30
template = "non-post-page.html"
[extra]
toc = true
+++

![About](about_ai.png)
> Research experience in data science, NLP, generative AI.

> Personality: ENTJ 

> Hobbits: Hiking, Swimming, Tennis, Piano, Reading, Traveling, Food

> Tech Skills: Python, R, SQL, AWS, S3, EC2, C++, Tableau, Machine Learning, NLP, Deep Learning, Hypothesis Testing, Statistical Inference, Regression Analysis.

> Coursework: Data Analysis at Scale in Cloud, Design and Analysis of Experiment, Statistical Models and Data Mining, Markov Chain Monte Carlo Algorithms, Data Structure & Algorithm in C++, Systems Programming & Engineering, NLP, Deep Learning, Theory of Machine Learning.

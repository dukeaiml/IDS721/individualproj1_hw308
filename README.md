# individualproj1_hw308

This is Isabella Wang's personal website including three portions: The home page, the self-introduction, and project description. 

### Website Link:

https://individualproj1-hw308-dukeaiml-ids721-bd2bf3cd2be9fb2fad6846d9a.gitlab.io


### Website Functionality & Demo Video

It has three portions, including self-introduction on home page, about me, and projects that I've participated in. Besides, it has the link to the corresponding projects, the functionality of keyword searching and website theme options. 

The `Demo Video` is in the root directory above called `Demo_video.webm`, please check it for all the functionalities. 


### Project Structure

Below is the structure of the project:

```bash
    project root

    ├── content
    ├── templates
    ├── themes
    ├── .gitlab-ci.yml
    ├── config.toml 
    ├── netlify.toml
    └── README.md

```
where the .gitlab-ci.yml helps the GitLab Runner to know how to create the site in order to deploy it to the GitLab Pages server. The GitLab CI/CD pipelines will ensure the site is published and updated automatically. [More details on Gitlab for Zola](https://www.getzola.org/documentation/deployment/gitlab-pages/)

where the netlify.toml help to deploy this project on [Netlify](https://www.netlify.com/)

### GitLab Workflow, Hosting and Deployment 

- Screenshots below show the passed pipeline, which shows the GitLab workflow to build and deploy site on push:

![](images/pipline0.png)

![](images/pipline1.png)

![](images/pipline2.png)

- Screenshots below show the site hosted on Netlify sucessfully: 

![](images/host.png)

where the link: https://startling-khapse-b120f3.netlify.app/


### Website Screenshots

- Home

![](images/1.png)

- About

![](images/2.png)

- Projects

![](images/3.png)

- IDS721: Data Analysis in Cloud Computing

![](images/4.png)




